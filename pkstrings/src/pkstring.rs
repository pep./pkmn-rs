// Copyright (C) 2021 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

//! Handles string encoding and decoding for the English version of Pokémon Gen 1.
//!
//! Documentation can be found at:
//! https://bulbapedia.bulbagarden.net/wiki/Character_encoding_(Generation_I)
//!
//! Control characters are considered valid but not handled yet.

use std::convert::TryFrom;
use std::fmt;
use std::error::Error as StdError;
use std::ops::Deref;

const fn in_range(ord: u8) -> bool {
    matches!(ord, 0x00 |
        0x49..=0x4c | 0x4e..=0x5f |  // Control characters
        0x60..=0x78 |  // Holdover from the japanese game
        0x79..=0x7f |  // Text box borders
        0x80..=0xbf |
        0xe0..=0xff)
}

const fn chrtohex(chr: &char) -> Option<u8> {
    Some(match chr {
        cap @ 'A'..='Z' => 0x80 - b'A' + (*cap as u8),
        '(' => 0x9a,
        ')' => 0x9b,
        ':' => 0x9c,
        ';' => 0x9d,
        '[' => 0x9e,
        ']' => 0x9f,
        low @ 'a'..='z' => 0xa0 - b'a' + (*low as u8),
        '\'' => 0xe0,
        '-' => 0xe3,
        '?' => 0xe6,
        '!' => 0xe7,
        '.' => 0xe8,
        '▷' => 0xec,
        '▶' => 0xed,
        '▼' => 0xee,
        '♂' => 0xef,
        '×' => 0xf1,
        // TODO: Pattern currently unreachable. Figure something out.
        // In the Japanese games (as can be seen below), 0xF2 is distinguishable from 0xE8, with
        // the former meant as a decimal point while the latter is punctuation. Presumably this
        // intention was largely inherited when the English games were made, as most of the game's
        // script uses 0xE8 exclusively; however, 0xF2 appears in the character table for user
        // input, meaning it may appear in user-input names (and, conversely, 0xE8 never should).
        // Source: https://bulbapedia.bulbagarden.net/wiki/Character_encoding_in_Generation_I
        // '.' => 0xf2,
        '/' => 0xf3,
        ',' => 0xf4,
        '♀' => 0xf5,
        num @ '0'..='9' => 0xf6 - b'0' + (*num as u8),
        ' ' => 0x7f,
        '@' => 0x50,
        _ => return None,
    })
}

const fn hextochr(hex: u8) -> Option<char> {
    Some(match hex {
        cap @ 0x80..=0x99 => (b'A' + (cap - 0x80)) as char,
        0x9a => '(',
        0x9b => ')',
        0x9c => ':',
        0x9d => ';',
        0x9e => '[',
        0x9f => ']',
        low @ 0xa0..=0xb9 => (b'a' + (low - 0xa0)) as char,
        0xe0 => '\'',
        0xe3 => '-',
        0xe6 => '?',
        0xe7 => '!',
        0xe8 => '.',
        0xec => '▷',
        0xed => '▶',
        0xee => '▼',
        0xef => '♂',
        0xf1 => '×',
        0xf2 => '.',
        0xf3 => '/',
        0xf4 => ',',
        0xf5 => '♀',
        num @ 0xf6..=0xff => (b'0' + (num - 0xf6)) as char,
        0x7f => ' ',
        0x50 => '@',
        _ => return None,
    })
}

#[derive(Debug, Eq, PartialEq)]
pub enum Error {
    InvalidByte(u8),
    InvalidCharacter(char),
}

impl StdError for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Error::InvalidByte(ord) => write!(f, "Invalid byte: {:?}", ord),
            Error::InvalidCharacter(chr) => write!(f, "Invalid character: {:?}", chr),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PKString(Vec<u8>);

impl Deref for PKString {
    type Target = Vec<u8>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl fmt::Display for PKString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", String::from(self))
    }
}

impl TryFrom<&[u8]> for PKString {
    type Error = Error;

    fn try_from(data: &[u8]) -> Result<PKString, Error> {
        let mut buf = Vec::with_capacity(data.len());

        for ord in data {
            if in_range(*ord) {
                buf.push(*ord);
            } else {
                return Err(Error::InvalidByte(*ord))
            }
        }

        Ok(PKString(buf))
    }
}

impl TryFrom<Vec<u8>> for PKString {
    type Error = Error;

    fn try_from(data: Vec<u8>) -> Result<PKString, Error> {
        for ord in &data {
            if ! in_range(*ord) {
                return Err(Error::InvalidByte(*ord))
            }
        }

        Ok(PKString(data))
    }
}

impl TryFrom<&str> for PKString {
    type Error = Error;

    fn try_from(data: &str) -> Result<PKString, Error> {
        let mut buf = Vec::new();

        for chr in data.chars() {
            match chrtohex(&chr) {
                Some(ord) => buf.push(ord),
                None => return Err(Error::InvalidCharacter(chr)),
            }
        }

        buf.shrink_to_fit();
        Ok(PKString(buf))
    }
}

impl From<PKString> for Vec<u8> {
    fn from(pkstr: PKString) -> Vec<u8> {
        pkstr.0
    }
}

impl From<&PKString> for String {
    fn from(pkstr: &PKString) -> String {
        let placeholder = '_';
        let mut buf = String::new();

        for ord in &pkstr.0 {
            match hextochr(*ord) {
                Some(chr) => buf.push(chr),
                None => buf.push(placeholder),
            }
        }

        buf
    }
}

impl PKString {
    pub fn as_slice(&self) -> &[u8] {
        self.0.as_slice()
    }
}
