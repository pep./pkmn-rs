// Copyright (C) 2021 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{PKString, Error};
use std::convert::TryFrom;

const GARY_SLICE_U8: &[u8] = &[0x86, 0x80, 0x91, 0x98];
const GARY_STR: &str = "GARY";
const INVALID_BYTE: u8 = 0x01;

#[test]
fn test_try_from_slice_u8() {
    match PKString::try_from(GARY_SLICE_U8) {
        Ok(pkstr) => assert_eq!(pkstr, PKString::try_from(GARY_STR).unwrap()),
        Err(_) => panic!(),
    }

    let invalid: &[u8] = &[
        0x86,
        0x80,
        INVALID_BYTE,
        0x98,
    ];
    match PKString::try_from(invalid) {
        Err(Error::InvalidByte(ord)) => assert_eq!(ord, INVALID_BYTE),
        _ => panic!(),
    }
}

#[test]
fn test_try_from_vec_u8() {
    let gary: Vec<u8> = GARY_SLICE_U8.to_vec();
    match PKString::try_from(gary) {
        Ok(pkstr) => assert_eq!(pkstr, PKString::try_from(GARY_STR).unwrap()),
        Err(_) => panic!(),
    }

    let invalid: Vec<u8> = vec![
        0x86,
        0x80,
        INVALID_BYTE,
        0x98,
    ];
    match PKString::try_from(invalid) {
        Ok(_) => panic!(),
        Err(_) => (),
    }
}

#[test]
fn test_from_ord() {
    assert_eq!(PKString::try_from(GARY_SLICE_U8), Ok(PKString::try_from(GARY_STR).unwrap()));

    let party_nicks: &[u8] = &[
        0x8a, 0x80, 0x83, 0x80, 0x81, 0x91, 0x80, 0x50, 0x50, 0x50, 0x50, 0x8d, 0x88, 0x83,
        0x8e, 0x8a, 0x88, 0x8d, 0x86, 0x50, 0x50, 0x50, 0x81, 0x8b, 0x80, 0x92, 0x93, 0x8e,
        0x88, 0x92, 0x84, 0x50, 0x50, 0x8e, 0x83, 0x83, 0x88, 0x92, 0x87, 0x50, 0x50, 0x50,
        0x50, 0x50, 0x8f, 0x88, 0x83, 0x86, 0x84, 0x98, 0x50, 0x50, 0x50, 0x50, 0x50, 0x82,
        0x87, 0x80, 0x91, 0x8c, 0x80, 0x8d, 0x83, 0x84, 0x91, 0x50,
    ];
    let result = "KADABRA@@@@NIDOKING@@@BLASTOISE@@ODDISH@@@@@PIDGEY@@@@@CHARMANDER@";
    assert_eq!(PKString::try_from(party_nicks), Ok(PKString::try_from(result).unwrap()));
}

#[test]
fn test_from_chr() {
    let result: Vec<u8> = PKString::try_from(GARY_STR).unwrap().into();
    assert_eq!(result, GARY_SLICE_U8);

    let nidoranf: Vec<u8> = vec![0x8d, 0x88, 0x83, 0x8e, 0x91, 0x80, 0x8d, 0xef];
    let result: Vec<u8> = PKString::try_from("NIDORAN♂").unwrap().into();
    assert_eq!(result, nidoranf);
}

#[test]
fn test_into_vec_u8() {
    let pkstr: PKString = PKString::try_from(GARY_SLICE_U8).unwrap();
    let res: Vec<u8> = pkstr.into();
    assert_eq!(res, GARY_SLICE_U8);
}

#[test]
fn test_as_slice_u8() {
    let pkstr: PKString = PKString::try_from(GARY_SLICE_U8).unwrap();
    let res: &[u8] = pkstr.as_slice();
    assert_eq!(res, GARY_SLICE_U8);
}
