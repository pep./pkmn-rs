// Copyright (C) 2020 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::env::args;
use std::convert::TryFrom;
use std::process::exit;

use pkstrings::PKString;

fn main() {
    let args: Vec<String> = args().collect();

    if args.len() < 2 {
        eprintln!("usage: {} <ascii>|rev <hex>", args[0]);
        exit(1);
    }

    if args.len() == 2 {
        // Convert String to PKString and then to Vec<u8>
        let tmp: Vec<u8> = PKString::try_from(args[1].as_str()).unwrap().into();
        println!("{:02x?}", tmp);
    } else if args.len() == 3 && args[1] == String::from("rev") {
        // Consure chars 2 by 2 and convert to u8.
        let mut vec: Vec<u8> = vec![];

        args[2].chars().fold(vec![], |mut acc, chr| {
            acc.push(chr);

            if acc.len() == 2 {
                let ord = format!("{}{}", acc[0], acc[1]);
                vec.push(u8::from_str_radix(&ord, 16).unwrap());
                acc = vec![];
            }

            acc
        });

        println!("{}", PKString::try_from(vec.as_slice()).unwrap());
    }
}
