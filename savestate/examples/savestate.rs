// Copyright (C) 2020 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use savestate::SaveState;
use std::convert::TryFrom;
use std::fs;

use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "savestate", about = "An example of savestate usage")]
struct Opt {
    #[structopt(parse(from_os_str), long, short)]
    files: Vec<PathBuf>,
}

fn main() {
    let opt = Opt::from_args();

    for file in opt.files {
        let data: Vec<u8> = fs::read(file).unwrap();
        println!("{:#?}\n", SaveState::try_from(data).unwrap());
    }
}
