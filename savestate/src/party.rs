// Copyright (C) 2020 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::convert::TryFrom;
use std::ops::Deref;

use crate::error::Error;
use crate::species::IndividualPk;

use pkstrings::PKString;

/// Offset of the first pokemon entry in the party
const PARTY_FIRST_ENTRY_OFFSET: usize = 0x9;

/// Size of a full pokemon entry
const PK_FULL_ENTRY_SIZE: usize = 44;

/// Original Trainer names offset in the party
const PARTY_OT_OFFSET: usize = 0x110;

/// Pokemon names offset in the party
const PARTY_PK_NAMES_OFFSET: usize = 0x152;

#[derive(Debug, PartialEq)]
pub struct PokemonList(Vec<IndividualPk>);

impl Deref for PokemonList {
    type Target = Vec<IndividualPk>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl TryFrom<&[u8]> for PokemonList {
    type Error = Error;

    fn try_from(data: &[u8]) -> Result<PokemonList, Error> {
        // First byte is the number of items in the list
        // Then each pokemon's species id is listed
        // Finally 0xFF to end the list.
        let count = data[0x0] as usize;

        if data[count + 1] != 0xFF {
            return Err(Error::InvalidListEnd);
        }

        // Original trainer names x6
        let mut ot_names: Vec<PKString> = vec![];
        let mut start = PARTY_OT_OFFSET;
        for _ in 0..count {
            let mut end = start + 1;
            while data[end] != 0x50 {
                end += 1;
            }
            ot_names.push(PKString::try_from(&data[start..end])?);
            start = end + 1;
        }

        // Pokemon names x6
        // Data seems to consist of 10 chars padded with 0x50 plus one 0x50 per pokémon.
        let mut pk_nicks: Vec<PKString> = vec![];
        let mut start = PARTY_PK_NAMES_OFFSET;
        for i in 1..=count {
            let mut end = start + 1;
            while data[end] != 0x50 {
                end += 1;
            }
            pk_nicks.push(PKString::try_from(&data[start..end])?);
            start = PARTY_PK_NAMES_OFFSET + 11 * i;
        }

        // Now pokemon data.
        let mut entries = vec![];
        for i in 1..=count {
            let start = PARTY_FIRST_ENTRY_OFFSET + ((i - 1) * PK_FULL_ENTRY_SIZE) - 1;
            let end = PARTY_FIRST_ENTRY_OFFSET - 1 + (i * PK_FULL_ENTRY_SIZE) - 1;
            let ot_name = ot_names[i - 1].clone();
            let pk_nick = pk_nicks[i - 1].clone();
            entries.push(IndividualPk::try_from((
                &data[start..=end],
                ot_name,
                pk_nick,
            ))?);
        }

        Ok(PokemonList(entries))
    }
}
