// Copyright (C) 2020 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/// Nintendo Pokemon gen1 save state handling, based on
/// https://bulbapedia.bulbagarden.net/wiki/Save_data_structure_in_Generation_I
pub mod error;
pub mod party;
pub mod species;

use std::convert::TryFrom;
use std::fmt;
use std::ops::Range;
use std::slice::Iter;

pub use crate::error::Error;
pub use crate::party::PokemonList;

use pkstrings::PKString;

/// Compute checksum of a specific region.
pub fn make_checksum(iter: Iter<u8>) -> u8 {
    iter.fold(255, |acc, x| acc.wrapping_sub(*x))
}

pub fn verify_checksum(iter: Iter<u8>, checksum: u8) -> bool {
    make_checksum(iter) == checksum
}

/// Party Data
const PARTY_DATA_RANGE: Range<usize> = 0x2F2C..0x30c0;

/// Main Data Checksum
const MAIN_DATA_CHECKSUM: usize = 0x3523;

/// Area validated by the Main Data Checksum
const MAIN_DATA_CHECKSUM_RANGE: Range<usize> = 0x2598..0x3523;

const PLAYER_NAME_OFFSET: usize = 0x2598;

/// A Save State
#[derive(PartialEq)]
pub struct SaveState {
    _data: Vec<u8>,

    /// Player name. offset: 0x2598, size: 0xB
    pub player_name: PKString,

    /// Party Data. offset: 0x2F2C, size: 0x194
    pub party: PokemonList,
}

impl fmt::Debug for SaveState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SaveState")
            .field("player_name", &self.player_name)
            .field("party", &self.party)
            .finish_non_exhaustive()
    }
}

impl TryFrom<Vec<u8>> for SaveState {
    type Error = Error;

    fn try_from(data: Vec<u8>) -> Result<SaveState, Error> {
        // Length of a savestate is 32k
        if data.len() != 32768 {
            return Err(Error::InvalidSaveLength);
        }

        if !verify_checksum(
            data[MAIN_DATA_CHECKSUM_RANGE].iter(),
            data[MAIN_DATA_CHECKSUM],
        ) {
            return Err(Error::InvalidChecksum);
        }

        let player_name = {
            let mut end = PLAYER_NAME_OFFSET;
            while data[end] != 0x50 {
                end += 1;
            }
            PKString::try_from(&data[PLAYER_NAME_OFFSET..end])?
        };

        Ok(SaveState {
            _data: data.clone(),
            player_name,
            party: PokemonList::try_from(&data[PARTY_DATA_RANGE])?,
        })
    }
}

impl SaveState {
    pub fn new(data: Vec<u8>) -> Result<SaveState, Error> {
        SaveState::try_from(data)
    }

    pub fn into_vec(self) -> Vec<u8> {
        self._data
    }
}
