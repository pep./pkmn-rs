// Copyright (C) 2020 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::convert::{TryFrom, TryInto};
use std::fmt;
use std::ops::{Deref, Range};

use crate::error::Error;

use lazy_static::lazy_static;
use pkstrings::PKString;

lazy_static! {
    // Names, types, etc.
    // Source: https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_index_number_(Generation_I)
    // TODO: Generate all this from the rom.

    static ref SPECIES_NAMES: Vec<Option<&'static str>> = {
        let tmp = [
            (0x1, "RHYDON"),
            (0x2, "KANGASHKAN"),
            (0x3, "NIDORAN♂"),
            (0x4, "CLEFAIRY"),
            (0x7, "NIDOKING"),
            (0x0e, "GENGAR"),
            (0x0f, "NIDORAN♀"),
            (0x13, "LAPRAS"),
            (0x1c, "BLASTOSE"),
            (0x24, "PIDGEY"),
            (0x26, "KADABRA"),
            (0x59, "DRAGONAIR"),
            (0x95, "ALAKAZAM"),
            (0xa7, "NIDORINO"),
            (0xb0, "CHARMANDER"),
            (0xb4, "CHARIZARD"),
            (0xb9, "ODDISH"),
        ];

        let mut vec: Vec<Option<&'static str>> = [None; 255].to_vec();
        for (k, v) in tmp.iter() {
            vec[*k] = Some(v);
        }
        vec
    };

    static ref SPECIES_TYPES: Vec<Option<PKTypes>> = {
        let tmp = [
            (0x1, PKTypes::Tuple(PKType::Ground, PKType::Rock)),
            (0x2, PKTypes::Single(PKType::Normal)),
            (0x3, PKTypes::Single(PKType::Poison)),
            (0x4, PKTypes::Single(PKType::Normal)),
            (0x7, PKTypes::Tuple(PKType::Poison, PKType::Ground)),
            (0x0e, PKTypes::Tuple(PKType::Ghost, PKType::Poison)),
            (0x0f, PKTypes::Single(PKType::Poison)),
            (0x13, PKTypes::Tuple(PKType::Water, PKType::Ice)),
            (0x1c, PKTypes::Single(PKType::Water)),
            (0x24, PKTypes::Tuple(PKType::Normal, PKType::Flying)),
            (0x26, PKTypes::Single(PKType::Psychic)),
            (0x59, PKTypes::Single(PKType::Dragon)),
            (0x95, PKTypes::Single(PKType::Psychic)),
            (0xa7, PKTypes::Single(PKType::Poison)),
            (0xb0, PKTypes::Single(PKType::Fire)),
            (0xb4, PKTypes::Tuple(PKType::Fire, PKType::Flying)),
            (0xb9, PKTypes::Tuple(PKType::Grass, PKType::Poison)),
        ];

        let mut vec: Vec<Option<PKTypes>> = [None; 255].to_vec();
        for (k, v) in tmp.iter() {
            vec[*k] = Some(*v);
        }
        vec
    };
}

const MISSINGNO_NAME: &'static str = "MissingNo";

/// Individual Values are Pokémon's equivalent of genes. They are instrumental in determining the
/// stats of a Pokémon, being responsible for the large variation in stats among untrained Pokémon
/// of the same species. IVs are fixed and cannot be changed.
///
/// Source: https://bulbapedia.bulbagarden.net/wiki/Individual_values
///
/// IVs are 0-15 values (0000-1111 binary).
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct IV {
    attack: u8,
    defense: u8,
    speed: u8,
    special: u8,
}

impl TryFrom<&[u8]> for IV {
    type Error = Error;

    fn try_from(data: &[u8]) -> Result<IV, Error> {
        let data: &[u8; 2] = data.try_into()?;
        IV::try_from(data)
    }
}

impl TryFrom<&[u8; 2]> for IV {
    type Error = Error;

    fn try_from(data: &[u8; 2]) -> Result<IV, Error> {
        // Binary Coded Decimal over 2 bytes
        Ok(IV {
            attack: (data[0x0] & 0xf0) >> 4,
            defense: data[0x0] & 0x0f,
            speed: (data[0x1] & 0xf0) >> 4,
            special: data[0x1] & 0x0f,
        })
    }
}

/// Effort Values, abbreviated as EVs, are values that contribute to an individual Pokémon's stats.
/// They are primarily obtained by defeating Pokémons in battle, based on the Pokémon that was
/// defeated.
///
/// Source: https://bulbapedia.bulbagarden.net/wiki/Effort_values
///
/// The maximum EV is 65535 for each stat.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct EV {
    hp: u16,
    attack: u16,
    defense: u16,
    speed: u16,
    special: u16,
}

impl TryFrom<&[u8]> for EV {
    type Error = Error;

    fn try_from(data: &[u8]) -> Result<EV, Error> {
        let data: &[u8; 10] = data.try_into()?;
        EV::try_from(data)
    }
}

impl TryFrom<&[u8; 10]> for EV {
    type Error = Error;

    fn try_from(data: &[u8; 10]) -> Result<EV, Error> {
        Ok(EV {
            hp: u16::from_be_bytes(data[0x0..0x2].try_into().unwrap()),
            attack: u16::from_be_bytes(data[0x2..0x4].try_into().unwrap()),
            defense: u16::from_be_bytes(data[0x4..0x6].try_into().unwrap()),
            speed: u16::from_be_bytes(data[0x6..0x8].try_into().unwrap()),
            special: u16::from_be_bytes(data[0x8..0xa].try_into().unwrap()),
        })
    }
}

/// Displayed stats. These can be recalculated from IVs and EVs.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Stats {
    hp: u16,
    attack: u16,
    defense: u16,
    speed: u16,
    special: u16,
}

impl TryFrom<&[u8]> for Stats {
    type Error = Error;

    fn try_from(data: &[u8]) -> Result<Stats, Error> {
        let data: &[u8; 10] = data.try_into()?;
        Ok(Stats::from(data))
    }
}

impl From<&[u8; 10]> for Stats {
    fn from(data: &[u8; 10]) -> Stats {
        Stats {
            hp: u16::from_be_bytes(data[0x0..0x2].try_into().unwrap()),
            attack: u16::from_be_bytes(data[0x2..0x4].try_into().unwrap()),
            defense: u16::from_be_bytes(data[0x4..0x6].try_into().unwrap()),
            speed: u16::from_be_bytes(data[0x6..0x8].try_into().unwrap()),
            special: u16::from_be_bytes(data[0x8..0xa].try_into().unwrap()),
        }
    }
}

/// Types are properties for Pokémons and their moves.
/// Source:
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum PKType {
    Normal = 0x0,
    Fighting = 0x1,
    Flying = 0x2,
    Poison = 0x3,
    Ground = 0x4,
    Rock = 0x5,
    Bug = 0x7,
    Ghost = 0x8,
    Fire = 0x14,
    Water = 0x15,
    Grass = 0x16,
    Electric = 0x17,
    Psychic = 0x18,
    Ice = 0x19,
    Dragon = 0x1a,
    Unknown = 0xff,
}

impl TryFrom<u8> for PKType {
    type Error = Error;

    fn try_from(data: u8) -> Result<PKType, Error> {
        Ok(match data {
            0x0 => PKType::Normal,
            0x1 => PKType::Fighting,
            0x2 => PKType::Flying,
            0x3 => PKType::Poison,
            0x4 => PKType::Ground,
            0x5 => PKType::Rock,
            0x7 => PKType::Bug,
            0x8 => PKType::Ghost,
            0x14 => PKType::Fire,
            0x15 => PKType::Water,
            0x16 => PKType::Grass,
            0x17 => PKType::Electric,
            0x18 => PKType::Psychic,
            0x19 => PKType::Ice,
            0x1a => PKType::Dragon,
            // _ => return Err(Error::InvalidPokemonTypes),
            _ => PKType::Unknown,
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum PKTypes {
    Single(PKType),
    Tuple(PKType, PKType),
    Unknown(u8, u8),
}

impl TryFrom<&[u8]> for PKTypes {
    type Error = Error;

    fn try_from(data: &[u8]) -> Result<PKTypes, Error> {
        let data: &[u8; 2] = data.try_into()?;
        PKTypes::try_from(data)
    }
}

impl TryFrom<&[u8; 2]> for PKTypes {
    type Error = Error;

    fn try_from(data: &[u8; 2]) -> Result<PKTypes, Error> {
        if data.len() != 2 {
            return Err(Error::InvalidPokemonTypes);
        }

        let type1 = PKType::try_from(data[0x0])?;
        let type2 = PKType::try_from(data[0x1])?;

        if type1 == type2 {
            Ok(PKTypes::Single(type1))
        } else {
            Ok(PKTypes::Tuple(type1, type2))
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum StatusConditions {
    None = 0x0,
    Asleep = 0x04,
    Poisoned = 0x08,
    Burned = 0x10,
    Frozen = 0x20,
    Paralyzed = 0x40,
}

impl TryFrom<u8> for StatusConditions {
    type Error = Error;

    fn try_from(data: u8) -> Result<StatusConditions, Error> {
        Ok(match data {
            0x0 => StatusConditions::None,
            0x4 => StatusConditions::Asleep,
            0x8 => StatusConditions::Poisoned,
            0x10 => StatusConditions::Burned,
            0x20 => StatusConditions::Frozen,
            0x40 => StatusConditions::Paralyzed,
            _ => return Err(Error::InvalidPokemonStatus),
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum PPUp {
    None,
    PPUp1,
    PPUp2,
    PPUp3,
}

impl TryFrom<u8> for PPUp {
    type Error = Error;

    fn try_from(data: u8) -> Result<PPUp, Error> {
        Ok(match data {
            0x0 => PPUp::None,
            0x1 => PPUp::PPUp1,
            0x2 => PPUp::PPUp2,
            0x3 => PPUp::PPUp3,
            n => return Err(Error::InvalidPokemonMovePPUp(n)),
        })
    }
}

#[derive(Clone, Copy, PartialEq)]
pub struct Move {
    id: u8,
    pp: u8,
    up: PPUp,
}

impl fmt::Debug for Move {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Move")
            .field("id", &format_args!("{:#02x}", self.id))
            .field("pp", &self.pp)
            .field("up", &self.up)
            .finish()
    }
}

impl TryFrom<(u8, u8)> for Move {
    type Error = Error;

    fn try_from((id, pp): (u8, u8)) -> Result<Move, Error> {
        // Gen1 doesn't have more than that number of moves
        if id > 165 {
            return Err(Error::InvalidPokemonMove);
        }

        Ok(Move {
            id: id,
            pp: pp & 0b00111111,
            up: PPUp::try_from((pp & 0b11000000) >> 6)?,
        })
    }
}

impl TryFrom<&[u8; 2]> for Move {
    type Error = Error;

    fn try_from(data: &[u8; 2]) -> Result<Move, Error> {
        Move::try_from((data[0], data[1]))
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Moves([Option<Move>; 4]);

impl Deref for Moves {
    type Target = [Option<Move>; 4];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl TryFrom<(&[u8], &[u8])> for Moves {
    type Error = Error;

    fn try_from(data: (&[u8], &[u8])) -> Result<Moves, Error> {
        let moves: &[u8; 4] = data.0.try_into()?;
        let pps: &[u8; 4] = data.1.try_into()?;
        Moves::try_from((moves, pps))
    }
}

impl TryFrom<(&[u8; 4], &[u8; 4])> for Moves {
    type Error = Error;

    fn try_from((moves, pps): (&[u8; 4], &[u8; 4])) -> Result<Moves, Error> {
        let array: [Option<Move>; 4] = [None; 4];
        let mut vec = array.to_vec();

        for i in 0..4 {
            if moves[i] == 0x0 {
                continue;
            }

            vec[i] = Some(Move::try_from((moves[i], pps[i]))?);
        }

        let tmp: [Option<Move>; 4] = vec.as_slice().try_into()?;
        Ok(Moves(tmp))
    }
}

impl From<[Option<Move>; 4]> for Moves {
    fn from(moves: [Option<Move>; 4]) -> Moves {
        Moves(moves)
    }
}

/// List of all valid Pokémon species.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Species {
    Rhydon,
    Kangaskhan,
    NidoranF,
    Clefairy,
    Nidoking,
    Gengar,
    NidoranM,
    Lapras,
    Blastoise,
    Pidgey,
    Kadabra,
    Dragonair,
    Alakazam,
    Nidorino,
    Charmander,
    Charizard,
    Oddish,
    MissingNo(u8),
    Invalid(u8),
}

impl Species {
    fn to_idx(&self) -> u8 {
        match *self {
            Species::MissingNo(n) => n,
            Species::Invalid(n) => n,
            Species::Rhydon => 0x1,
            Species::Kangaskhan => 0x2,
            Species::NidoranF => 0x3,
            Species::Clefairy => 0x4,
            Species::Nidoking => 0x7,
            Species::Gengar => 0x0e,
            Species::NidoranM => 0x0f,
            Species::Lapras => 0x13,
            Species::Blastoise => 0x1c,
            Species::Pidgey => 0x24,
            Species::Kadabra => 0x26,
            Species::Dragonair => 0x59,
            Species::Alakazam => 0x95,
            Species::Nidorino => 0xa7,
            Species::Charmander => 0xb0,
            Species::Charizard => 0xb4,
            Species::Oddish => 0xb9,
        }
    }

    fn from_idx(idx: u8) -> Self {
        match idx {
            0x1 => Species::Rhydon,
            0x2 => Species::Kangaskhan,
            0x3 => Species::NidoranF,
            0x4 => Species::Clefairy,
            0x7 => Species::Nidoking,
            0x0e => Species::Gengar,
            0x0f => Species::NidoranM,
            0x13 => Species::Lapras,
            0x1c => Species::Blastoise,
            0x24 => Species::Pidgey,
            0x26 => Species::Kadabra,
            0x59 => Species::Dragonair,
            0x95 => Species::Alakazam,
            0xa7 => Species::Nidorino,
            0xb0 => Species::Charmander,
            0xb4 => Species::Charizard,
            0xb9 => Species::Oddish,
            n if (0xbe..=0xffu8).contains(&n) => Species::Invalid(n),
            n => Species::MissingNo(n),
        }
    }

    pub fn name(&self) -> &'static str {
        match SPECIES_NAMES[self.to_idx() as usize] {
            Some(ref name) => name,
            None => MISSINGNO_NAME,
        }
    }

    pub fn types(&self) -> PKTypes {
        match SPECIES_TYPES[self.to_idx() as usize] {
            Some(types) => types.clone(),
            // MissingNo have no types but use the same as the last pokemon in memory.
            None => panic!("No pokemon type available"),
        }
    }
}

impl From<u8> for Species {
    fn from(idx: u8) -> Species {
        Species::from_idx(idx)
    }
}

const PK_SPECIES_OFFSET: usize = 0x0;
const PK_CURRENT_HP_RANGE: Range<usize> = 0x1..0x3;
const PK_LEVEL_OFFSET: usize = 0x21;
const PK_STATUS_OFFSET: usize = 0x4;
const PK_TYPES_OFFSET: Range<usize> = 0x5..0x7;
const PK_CATCH_RATE_OFFSET: usize = 0x7;
const PK_EV_RANGE: Range<usize> = 0x11..0x1b;
const PK_IV_RANGE: Range<usize> = 0x1b..0x1d;
const PK_XP_OFFSET: usize = 0xe; // Over 3 bytes
const PK_OT_RANGE: Range<usize> = 0xc..0xe;
const PK_MOVES_RANGE: Range<usize> = 0x8..0xc;
const PK_PP_RANGE: Range<usize> = 0x1d..0x21;
const PK_STATS_RANGE: Range<usize> = 0x22..0x2c;

#[derive(PartialEq)]
pub struct IndividualPk {
    _data: Vec<u8>,
    pub species: Species,
    pub nickname: PKString,
    pub current_hp: u16,
    pub level: u8,
    pub status: StatusConditions,
    pub catch_rate: u8,
    pub original_trainer_id: u16,
    pub original_trainer_name: PKString,
    pub xp: u32,
    pub iv: IV,
    pub ev: EV,
    pub stats: Stats,
    pub moves: Moves,
}

impl fmt::Debug for IndividualPk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("IndividualPk")
            .field("species", &self.species)
            .field("nickname", &self.nickname)
            .field("current_hp", &self.current_hp)
            .field("level", &self.level)
            .field("status", &self.status)
            .field("catch_rate", &self.catch_rate)
            .field("original_trainer_id", &self.original_trainer_id)
            .field("original_trainer_name", &self.original_trainer_name)
            .field("xp", &self.xp)
            .field("iv", &self.iv)
            .field("ev", &self.ev)
            .field("stats", &self.stats)
            .field("moves", &self.moves)
            .finish_non_exhaustive()
    }
}

impl TryFrom<(&[u8; 44], PKString, PKString)> for IndividualPk {
    type Error = Error;

    fn try_from(
        (data, ot_name, pk_nick): (&[u8; 44], PKString, PKString),
    ) -> Result<IndividualPk, Error> {
        IndividualPk::try_from((&data[..], ot_name, pk_nick))
    }
}

impl TryFrom<(&[u8], PKString, PKString)> for IndividualPk {
    type Error = Error;

    fn try_from(
        (data, ot_name, pk_nick): (&[u8], PKString, PKString),
    ) -> Result<IndividualPk, Error> {
        let species = Species::from(data[PK_SPECIES_OFFSET]);
        let current_hp: u16 = u16::from_be_bytes(data[PK_CURRENT_HP_RANGE].try_into().unwrap());
        let types = PKTypes::try_from(&data[PK_TYPES_OFFSET])?;
        if species.types() != types {
            return Err(Error::InvalidPokemonTypes);
        }
        let xp: u32 = {
            (data[PK_XP_OFFSET] as u32) << 16
                | (data[PK_XP_OFFSET + 0x1] as u32) << 8
                | (data[PK_XP_OFFSET + 0x2] as u32)
        };
        let original_trainer_id: u16 = u16::from_be_bytes(data[PK_OT_RANGE].try_into().unwrap());
        let moves = Moves::try_from((&data[PK_MOVES_RANGE], &data[PK_PP_RANGE]))?;

        Ok(IndividualPk {
            _data: data.to_vec(),
            species,
            nickname: pk_nick,
            current_hp,
            level: data[PK_LEVEL_OFFSET],
            status: StatusConditions::try_from(data[PK_STATUS_OFFSET])?,
            catch_rate: data[PK_CATCH_RATE_OFFSET],
            original_trainer_id,
            original_trainer_name: ot_name,
            xp,
            iv: IV::try_from(&data[PK_IV_RANGE])?,
            ev: EV::try_from(&data[PK_EV_RANGE])?,
            stats: Stats::try_from(&data[PK_STATS_RANGE])?,
            moves,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::species::{
        IndividualPk, Move, Moves, PKType, PKTypes, PPUp, Species, StatusConditions, EV, IV,
    };
    use pkstrings::PKString;
    use std::convert::TryFrom;

    const KADABRA: [u8; 44] = [
        // Species id
        0x26, // Current HP
        0x0, 0x2c, // 44
        0x0,  // Level ?!
        0x0,  // Status condition
        0x18, 0x18, // Pokémon types: Psychic
        // Catch rate
        0xc8, // 200
        // Index of Moves 1, 2, 3, 4
        0x64, 0x5d, 0x0, 0x0, // Original Trainer ID
        0xba, 0x26, // 47654
        // Experience points
        0x0, 0xa, 0x3a, // 2618
        // HP EV data
        0x0, 0xf8, // 248
        // Attack EV data
        0x1, 0x36, // 310
        // Defense EV data
        0x0, 0xef, // 239
        // Speed EV data
        0x0, 0xc5, // 197
        // Special EV data
        0x1, 0x6d, // 365
        // IV data
        0x7b, 0x17, // 7, 11, 1, 7
        // PP Move 1, 2, 3, 4. Lower 6 bits are PPs, higher 2 bits are the number of PP Ups applied
        // to the move.
        0x14, 0x19, 0x0, 0x0, // (20, 0), (25, 0), (0, 0), (0, 0)
        // Level
        0x10, // 16
        // HP stat
        0x0, 0x2c, // 44
        // Attack stat
        0x0, 0x13, // 19
        // Defense stat
        0x0, 0x12, // 18
        // Speed stat
        0x0, 0x27, // 39
        // Special stat
        0x0, 0x2e, // 46
    ];

    const CHARIZARD: [u8; 44] = [
        0xb4, 0x0, 0x8b, 0x0, 0x0, 0x14, 0x2, 0x2d, 0xa3, 0x34, 0x63, 0xf, 0x6, 0x81, 0x1, 0x14,
        0x83, 0x28, 0xf3, 0x47, 0xbc, 0x2b, 0x32, 0x39, 0xfb, 0x3c, 0x3d, 0xf3, 0x94, 0x14, 0x19,
        0x14, 0x1e, 0x2a, 0x0, 0x8b, 0x0, 0x66, 0x0, 0x53, 0x0, 0x6d, 0x0, 0x5c,
    ];

    #[test]
    fn test_species_simple() {
        let species = Species::from(KADABRA[0x0]);
        assert_eq!(species, Species::Kadabra);
        assert_eq!(species.name(), "KADABRA");
        assert_eq!(species.types(), PKTypes::Single(PKType::Psychic));

        let species = Species::from(CHARIZARD[0x0]);
        assert_eq!(species, Species::Charizard);
        assert_eq!(species.name(), "CHARIZARD");
        assert_eq!(
            species.types(),
            PKTypes::Tuple(PKType::Fire, PKType::Flying)
        );
    }

    #[test]
    fn test_individual_simple() {
        let ot_name: PKString = PKString::try_from("FOOBAR").unwrap();
        let kadabra_nick: PKString = PKString::try_from("KADABRA").unwrap();
        let charizard_nick: PKString = PKString::try_from("CHARIZARD").unwrap();

        let pk = IndividualPk::try_from((&KADABRA, ot_name.clone(), kadabra_nick)).unwrap();
        assert_eq!(pk.species, Species::Kadabra);
        assert_eq!(pk.current_hp, 44);
        assert_eq!(pk.level, 16);
        assert_eq!(pk.status, StatusConditions::None);
        assert_eq!(pk.catch_rate, 200);
        assert_eq!(
            pk.iv,
            IV {
                attack: 7,
                defense: 11,
                speed: 1,
                special: 7
            }
        );
        assert_eq!(
            pk.ev,
            EV {
                hp: 248,
                attack: 310,
                defense: 239,
                speed: 197,
                special: 365
            }
        );
        assert_eq!(pk.xp, 2618);
        assert_eq!(pk.original_trainer_id, 47654);
        assert_eq!(
            pk.original_trainer_name,
            PKString::try_from("FOOBAR").unwrap()
        );
        assert_eq!(
            pk.moves,
            [
                Some(Move {
                    id: 0x64,
                    pp: 20,
                    up: PPUp::None
                }),
                Some(Move {
                    id: 0x5d,
                    pp: 25,
                    up: PPUp::None
                }),
                None,
                None,
            ]
            .into()
        );

        let pk = IndividualPk::try_from((&CHARIZARD, ot_name, charizard_nick)).unwrap();
        assert_eq!(pk.species, Species::Charizard);
        assert_eq!(pk.current_hp, 139);
        assert_eq!(pk.level, 42);
        assert_eq!(pk.status, StatusConditions::None);
        assert_eq!(pk.catch_rate, 45);
        assert_eq!(
            pk.iv,
            IV {
                attack: 15,
                defense: 3,
                speed: 9,
                special: 4
            }
        );
        assert_eq!(
            pk.ev,
            EV {
                hp: 10483,
                attack: 18364,
                defense: 11058,
                speed: 14843,
                special: 15421
            }
        );
        assert_eq!(pk.xp, 70787);
        assert_eq!(pk.original_trainer_id, 1665);
        assert_eq!(
            pk.original_trainer_name,
            PKString::try_from("FOOBAR").unwrap()
        );
        assert_eq!(
            pk.moves,
            [
                Some(Move {
                    id: 0xa3,
                    pp: 20,
                    up: PPUp::None
                }),
                Some(Move {
                    id: 0x34,
                    pp: 25,
                    up: PPUp::None
                }),
                Some(Move {
                    id: 0x63,
                    pp: 20,
                    up: PPUp::None
                }),
                Some(Move {
                    id: 0x0f,
                    pp: 30,
                    up: PPUp::None
                }),
            ]
            .into()
        );
    }

    #[test]
    fn test_move_try_from() {
        let data1: (u8, u8) = (0x65, 0x52);
        let move1 = Move::try_from(data1).unwrap();
        let data2: &[u8; 2] = &[0x65, 0x52];
        let move2 = Move::try_from(data2).unwrap();
        assert_eq!(
            move1,
            Move {
                id: 0x65,
                pp: 18,
                up: PPUp::PPUp1
            }
        );
        assert_eq!(move1, move2);
    }

    #[test]
    fn test_move_ppup() {
        let data: [u8; 2] = [0x65, 0x52];
        let move1 = Move::try_from(&data).unwrap();
        assert_eq!(
            move1,
            Move {
                id: 0x65,
                pp: 18,
                up: PPUp::PPUp1
            }
        );
    }

    #[test]
    fn test_moves_try_from() {
        let moves: [u8; 4] = [0x64, 0x5d, 0x0, 0x0];
        let pps: [u8; 4] = [0x14, 0x19, 0x0, 0x0];
        let results = Moves::try_from((&moves, &pps)).unwrap();
        assert_eq!(
            results,
            [
                Some(Move {
                    id: 0x64,
                    pp: 20,
                    up: PPUp::None
                }),
                Some(Move {
                    id: 0x5d,
                    pp: 25,
                    up: PPUp::None
                }),
                None,
                None,
            ]
            .into()
        );
    }
}
