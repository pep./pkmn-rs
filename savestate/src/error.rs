// Copyright (C) 2020 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::fmt;
use pkstrings::Error as PKStringError;
use std::array::TryFromSliceError;
use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    InvalidSaveLength,
    InvalidChecksum,
    InvalidListEnd,
    InvalidPokemonTypes,
    InvalidPokemonStatus,
    InvalidPokemonMove,
    InvalidPokemonMovePPUp(u8),
    IO(IOError),
    Array(TryFromSliceError),
    PKString(PKStringError),
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Error {
        Error::IO(err)
    }
}

impl From<TryFromSliceError> for Error {
    fn from(err: TryFromSliceError) -> Error {
        Error::Array(err)
    }
}

impl From<PKStringError> for Error {
    fn from(err: PKStringError) -> Error {
        Error::PKString(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Error::InvalidSaveLength => write!(f, "Invalid save length"),
            Error::InvalidChecksum => write!(f, "Invalid checksum"),
            Error::InvalidListEnd => write!(f, "Invalid expected end of list"),
            Error::InvalidPokemonTypes => write!(f, "Invalid Pokémon Types"),
            Error::InvalidPokemonStatus => write!(f, "Invalid Pokémon Status"),
            Error::InvalidPokemonMove => write!(f, "Invalid Pokémon Move"),
            Error::InvalidPokemonMovePPUp(pp) => write!(f, "Invalid Pokémon PP: {:x?}", pp),
            Error::IO(err) => write!(f, "{}", err),
            Error::Array(err) => write!(f, "{}", err),
            Error::PKString(err) => write!(f, "{}", err),
        }
    }
}
